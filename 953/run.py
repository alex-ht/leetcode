class Solution(object):
    def isAlienSorted(self, words, order):
        """
        :type words: List[str]
        :type order: str
        :rtype: bool
        """
        table = {}
        for i in range(26):
            table[order[i]] = i
        
        for wid in range(1, len(words)):
            for pos in range(20):
                prev_char_order = table[words[wid-1][pos]] if len(words[wid-1]) > pos else -1
                curr_char_order = table[words[wid][pos]] if len(words[wid]) > pos else -1
                if (curr_char_order > prev_char_order):
                    break
                if (curr_char_order < prev_char_order):
                    return False
        return True