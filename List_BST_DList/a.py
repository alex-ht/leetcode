class Node:
    def __init__(self, val=None, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class BST:
    def __init__(self, ary):
        self.root = Node(ary[0])
        for x in ary[1:]:
            curr = self.root
            while True:
                if curr.val > x :
                    if curr.left is None:
                        curr.left = Node(x)
                        break
                    else:
                        curr = curr.left
                else:
                    if curr.right is None:
                        curr.right = Node(x)
                        break
                    else:
                        curr = curr.right

    def inorder(self, node):
        yield from self.inorder(node.left) if node.left is not None else ()
        yield node.val
        yield from self.inorder(node.right) if node.right is not None else ()


class DLinkedList:
    def __init__(self, gen, size):
        self.begin = Node(next(gen))
        curr = self.begin
        for x in range(size-1):
            curr.right = Node(next(gen), left=curr)
            curr = curr.right
        self.end = curr

    def print(self):
        curr = self.begin
        result = []
        while curr is not None:
            result.append(curr.val)
            curr = curr.right
        print(result)


def main():
    # array to double linked list
    bst = BST([0, 9, 7, 2])
    gen = bst.inorder(bst.root)
    dl = DLinkedList(gen, 4)
    dl.print()


if __name__ == '__main__':
    main()