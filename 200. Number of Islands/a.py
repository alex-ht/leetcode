# Runtime: 64 ms, faster than 93.39% of Python3 online submissions for Number of Islands.
class Solution:
    def find_adj_one(self, grid, i, j):
        next_move = []
        if i - 1 >= 0 and grid[i-1][j] == "1":
            grid[i-1][j] = 0
            next_move.append((i-1, j))
        if i + 1 < len(grid) and grid[i+1][j] == "1":
            grid[i+1][j] = 0
            next_move.append((i+1, j))
        if j - 1 >= 0 and grid[i][j-1] == "1":
            grid[i][j-1] = 0
            next_move.append((i, j-1))
        if j + 1 < len(grid[0]) and grid[i][j+1] == "1":
            grid[i][j+1] = 0
            next_move.append((i, j+1))
        for a,b in next_move:
            self.find_adj_one(grid, a, b)
        
    def numIslands(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        nr = len(grid)
        if nr == 0:
            return 0
        nf = len(grid[0])
        count = 0
        for i in range(nr):
            for j in range(nf):
                if grid[i][j] == "1":
                    count = count + 1
                    grid[i][j] = 0
                    self.find_adj_one(grid, i, j)
        return count
                    