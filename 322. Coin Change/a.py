""" Time Limit Exceeded """
class Solution:
    def __init__(self):
        self.coins = None
        self.table = {}
        
    def change(self, amount):
        if amount in self.table:
            return self.table[amount]
        if amount == 0:
            return 0
        can_change = [ x for x in self.coins if amount >= x ]
        if len(can_change) == 0:
            self.table[amount] = False
            return False
        min_value = None
        for x in can_change:
            y = self.change(amount - x)
            if y is not False:
                if min_value == None:
                    min_value = y
                else:
                    if y <min_value:
                        min_value = y
        
        if min_value == None:
            self.table[amount] = False
            return False
        
        self.table[amount] = min_value + 1
        return self.table[amount]
        
    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        if amount == 0:
            return 0
        
        self.coins = coins
        self.table = {}
        y = self.change(amount)
        if y == None:
            return -1
        if y is False:
            return -1
        return y