// Runtime: 16 ms, faster than 41.75% of C++ online submissions for Convert Sorted Array to Binary Search Tree.
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

struct Task {
    int first;
    int last;
    TreeNode *node;
    Task(int f, int l, TreeNode* n): first(f), last(l), node(n){}
};

class Solution {
public:
    TreeNode* sortedArrayToBST(vector<int>& nums) {
        //vector<bool> dirty_bits(nums.size(), true);
        if (nums.empty()) return NULL;
        TreeNode* root = new TreeNode(INT_MAX);
        queue<Task> tasks;
        tasks.push(Task(0, nums.size()-1, root));
        while(!tasks.empty()) {
            auto t = tasks.front();
            int middle = (t.first + t.last) / 2;
            t.node->val = nums[middle];
            if (middle > t.first) {
                t.node->left = new TreeNode(INT_MAX);
                tasks.push(Task(t.first, middle-1, t.node->left));
            }
            if (middle < t.last) {
                t.node->right = new TreeNode(INT_MAX);
                tasks.push(Task(middle+1, t.last, t.node->right));
            }
            tasks.pop();
        }
        return root;
    }
};