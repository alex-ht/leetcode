# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def rightSideView(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        if root is None:
            return []
        # 取得每個level的數值
        task_q = [(root, 0)]
        layers = []
        while len(task_q)>0:
            node, level = task_q[0]
            if len(layers) <= level:
                layers.append([node.val])
            else:
                layers[level].append(node.val)
            if node.left is not None:
                task_q.append((node.left, level + 1))
            if node.right is not None:
                task_q.append((node.right, level + 1))
            task_q.pop(0)
        
        return [ x[-1] for x in layers]