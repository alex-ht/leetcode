class Solution:
    def minDistance(self, word1, word2):
        """
        :type word1: str
        :type word2: str
        :rtype: int
        """
        wl1 = len(word1)
        wl2 = len(word2)
        table = [[0] * (wl2 + 1)  for h in range(wl1 + 1) ]
        
        for i in range(1,wl2+1):
            table[0][i] = i
            
        for i in range(1,wl1+1):
            table[i][0] = i
        
        for row in range(1, wl1+1):
            for col in range(1, wl2+1):
                table[row][col] = min(
                    table[row -1][col] + 1, 
                    table[row][col-1] + 1, 
                    table[row-1][col-1] + (1 if word1[row-1] != word2[col-1] else 0))
                    
        return table[-1][-1]
        