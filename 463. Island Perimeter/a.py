class Solution:
    def islandPerimeter(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        height = len(grid)
        width = len(grid[0])
        grid = [[0] + x + [0] for x in grid]
        grid.insert(0, [0] * (width + 2))
        grid.append([0] * (width + 2))
        total = 0
        for col in range(width+1):
            for row in range(height+1):
                if grid[row][col] != grid[row+1][col]:
                    total=total+1
                if grid[row][col] != grid[row][col+1]:
                    total=total+1
        return total