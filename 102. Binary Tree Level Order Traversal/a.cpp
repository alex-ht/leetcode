/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

typedef pair<TreeNode*, int> Task;

class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> solution;
        if (root == NULL) return solution;
        queue< Task > tasks;
        tasks.push(Task(root, 0));
        while(!tasks.empty()) {
            Task t = tasks.front();
            tasks.pop();
            if (solution.size() <= t.second) {
                solution.push_back(vector<int>(1, t.first->val));
            } else {
                solution.at(t.second).push_back(t.first->val);
            }
            if (t.first->left != NULL) 
                tasks.push(Task(t.first->left, t.second + 1));
            if (t.first->right != NULL)
                tasks.push(Task(t.first->right, t.second + 1));
        }
        return solution;
    }
};