# Runtime: 56 ms, faster than 84.13% of Python3 online submissions for Minimum Path Sum.
class Solution:
    def minPathSum(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        grid = [[2**32] + x for x in grid]
        grid.insert(0, [2**32] * len(grid[0]))
        grid[0][1] = 0
        for row in range(1, len(grid)):
            for col in range(1, len(grid[0])):
                grid[row][col] += min(grid[row-1][col], grid[row][col-1])
        return grid[-1][-1]