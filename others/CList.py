# -*- coding: utf-8 -*-

class ListNode:
    def __init__(self, val):
        self.val = val
        self.prev = None
        self.next = None

class DoublyLinkedList:

    def __init__(self):
        self._length = 0
        self.head = None

    def __len__(self):
        return self._length

    def append(self, val):
        if val is not None:
            self._length += 1
            node = ListNode(val)
            if self.head:
                node.prev, node.next = self.head.prev, self.head
                node.prev.next, node.next.prev = node, node
            else:
                node.prev, node.next = node, node
                self.head = node

    def sort(self):
        if self._length < 2: return
        
        node = self.head
        node.prev.next = None
        node.prev = None
        forward = self._sortLinkedList(node)
        previous = None
        start = current = forward

        while current:
            current.prev = previous
            previous = current
            current = current.next
        
        if start and previous:
            start.prev = previous
            previous.next = start
        self.head = start
        

    def _sortLinkedList(self, node):
        if not node or not node.next: return node
        
        prev, slow, fast = None, node, node

        while fast and fast.next:
            prev = slow
            slow = slow.next
            fast = fast.next.next

        if prev:
            prev.next = None

        # print('start:{}, mid:{}'.format(singleLinkedListTraverse(node), singleLinkedListTraverse(slow)))
        left = self._sortLinkedList(node)
        right = self._sortLinkedList(slow)

        return self._merge(left, right)

    def _merge(self, l, r):
        if not l or not r:
            return l or r
        if l.val > r.val:
            l, r = r, l

        head = pre = l
        l = l.next

        while l and r:
            if l.val < r.val:
                pre.next = l
                l = l.next
            else:
                pre.next = r
                r = r.next
            pre = pre.next
        if not l or not r:
            pre.next = l or r
        return head


def singleLinkedListTraverse(node):
    if not node: return '[]'
    
    current, ret = node, []
    while current:
        ret.append(current.val)
        current = current.next

    return '[{}]'.format(', '.join(map(str, ret)))

def forwardTraverse(node):
    if not node: return '[]'
    
    start = current = node
    ret = [current.val]
    current = current.next

    while current != start:
        ret.append(current.val)
        current = current.next

    return '[{}]'.format(', '.join(map(str, ret)))

def backwardTraverse(node):
    if not node: return '[]'
    
    start = current = node
    ret = [current.val]
    current = current.prev

    while current != start:
        ret.append(current.val)
        current = current.prev

    return '[{}]'.format(', '.join(map(str, ret)))


def _testTraverse(nums):
    clist = DoublyLinkedList()
    for num in nums:
        clist.append(num)
    print('len: {}\nforward: {}\nbackward: {}'.format(len(clist), forwardTraverse(clist.head), backwardTraverse(clist.head)))

    clist.sort()
    print('len: {}\nforward: {}\nbackward: {}'.format(len(clist), forwardTraverse(clist.head), backwardTraverse(clist.head)))


def main():
    _testTraverse([2, 3, 1, 4, 5])
    _testTraverse([])
    _testTraverse([2])

if __name__ == '__main__':
    main()