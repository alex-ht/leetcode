# -*- coding:utf-8 -*-
import collections

def minOperation(p):
    if not p: return 0
    n = len(p) - 1
    t = [[0] * n for _ in range(n)]

    d, name = collections.defaultdict(str), ord('A')
    for i in range(n):
        d[i, i] = '{}'.format(chr(name))
        if i < n-1:
            d[i, i+1] = '({}{})'.format(chr(name), chr(name+1))
        name += 1

    for i in range(n, -1, -1):
        for j in range(i, n):
            if i == j: continue
            for k in range(i, j):
                tmp = t[i][k] + t[k+1][j] + p[i] * p[k+1] * p[j+1]
                if tmp < t[i][j] or not t[i][j]:
                    t[i][j] = tmp
                    d[i, j] = '({}{})'.format(d[i, k], d[k+1, j])
    # print(d)
    return t[0][-1], d[0, n-1]

def main():
    print('{1}, sum={0}'.format(*minOperation([40, 20, 30, 10, 30])))
    print('{1}, sum={0}'.format(*minOperation([10, 20, 30, 40, 30])))
    print('{1}, sum={0}'.format(*minOperation([10, 20, 30])))

if __name__ == '__main__':
    main()
