class Solution:
    def lengthOfLIS(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0
        seq = [nums[0]]
        for x in nums[1:]:
            if x > seq[-1]:
                seq.append(x)
                continue
            for i in range(len(seq)):
                if x <= seq[i]:
                    seq[i] = x;
                    break
        return len(seq)

assert Solution().lengthOfLIS([10,9,2,5,3,7,101,18]) == 4