# Implement a constant arithmetic expression evaluator.
# The input is a string of math expression containing only constant math terms (i.e., no variables).


class Op:
    def __init__(self, priority=-1, symbol = '', function = None, operand1 = None, operand2 = None):
        self.priority = priority
        self.symbol = symbol
        self.function = function
        self.operand1 = operand1
        self.operand2 = operand2

    def eval(self):
        return self.function(self.operand1, self.operand2)


class Value:
    def __init__(self, val = None):
        self.val = val

    def eval(self):
        try :
            float(self.val)
            return self.val
        except ValueError:
            return self.val.eval()


def define_operators():
    return {
        '+': Op(priority=13, symbol='+', function=lambda a, b: a.eval() + b.eval()),
        '-': Op(priority=13, symbol='-', function=lambda a, b: a.eval() - b.eval()),
        '*': Op(priority=14, symbol='*', function=lambda a, b: a.eval() * b.eval()),
        '/': Op(priority=14, symbol='/', function=lambda a, b: a.eval() / b.eval())
    }

class Expression:
    def __

def main():
    ops = define_operators()