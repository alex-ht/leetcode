//Runtime: 72 ms, faster than 4.95% of C++ online submissions for ZigZag Conversion.
typedef pair<int, int> Pos;

class Solution {
public:
    string convert(string s, int numRows) {
        if (numRows == 1 || s.length() <= numRows) {
            return s;
        }
        vector<vector<char> > table(numRows, vector<char>(s.length(), '_'));
        Pos p(0,0);
        for(auto i = s.begin();i!=s.end();i++) {
            table[p.first][p.second] = *i;
            // move p
            if (p.second % (numRows-1) == 0) {
                if (p.first < (numRows - 1)) { // go down
                    p.first ++;
                } else { // up-right
                    p.first --;
                    p.second ++;
                }
            } else { // up-right
                p.first --;
                p.second ++;
            }
        }
        string str;
        for(auto row = table.begin();row!=table.end();row++) {
            for(auto c = row->begin();c!=row->end();c++) {
                if (*c != '_') {
                    str += *c;
                }
            }
        }
        return str;
    }
};