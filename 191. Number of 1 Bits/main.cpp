#include <iostream>
#include <cassert>
using namespace std;

struct FourByte {
    unsigned char byte[4];
};

class Solution {
public:
    Solution() {
        for (uint32_t i=0;i<256;i++) {
            uint32_t j = i ;
            int sum = 0;
            for (int k=0;k<8;k++){
                sum += j % 2;
                j = j >> 1;
            }
            table[i] = sum;
        }
    }
    int hammingWeight(uint32_t n) {
        FourByte *x = reinterpret_cast<FourByte *> (&n);
        return table[x->byte[0]] + table[x->byte[1]] + table[x->byte[2]] + table[x->byte[3]];
    }
    int table[256];
};

int main() {
    Solution s;
    assert(s.hammingWeight(11) == 3);
    assert(s.hammingWeight(128) == 1);
    cout << "Pass";
    return 0;
}
